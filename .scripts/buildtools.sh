#!/bin/bash

# functions to work with SpigotMC's BuildTools

source .scripts/util.sh

is_spigot_jar() {
  test -f spigot*.jar
  return $?
}

find_spigot_jar() {
  local spigot_jar=$(echo $(find spigot*.jar | cut -f 1) | cut -d " " -f 1)
  if ! cmd_success; then
    return 1
  fi
  echo $spigot_jar
  return 0
}

bt_download() {
  echo "Downloading BuildTools ..."

  wget_dl "https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar" "$build_tools_jar"
  if ! cmd_success; then
    echo "Download error!"
    cd $wd
    return 1
  fi
  chmod u+x $build_tools_jar
  echo "BuildTools downloaded!"
  return 0
}

bt_compile() {
  if !(package_check 1 "openjdk-8-jdk"); then
    echo "The JDK 8 must be installed in order to compile spigot!"
    return 1
  fi

  echo "Compiling Spigot ..."

  java -jar BuildTools.jar &> /dev/null
  cat BuildTools.log.txt | grep -q "Success! Everything completed successfully. Copying final .jar files now."

  if ! cmd_success; then
    echo "Compilation error!"
    cd $wd
    return 1
  fi
}

# $1: file_destination
cp_spigot_jar() {
  local build_dir=.build
  local build_tools_jar="BuildTools.jar"

  if ! [ -d $build_dir ]; then
    mkdir $build_dir
  fi

  local wd=$PWD
  cd $build_dir

  if ! [ -f $build_tools_jar ]; then
    bt_download
  fi

  if ! is_spigot_jar; then
    bt_compile
  fi
  local spigot_jar=$(find_spigot_jar)

  cd $wd
  cp $build_dir/$spigot_jar $1
  return $?
}
