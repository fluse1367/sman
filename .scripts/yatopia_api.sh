#!/bin/bash

# functions to access yatopiamc.org's api

source .scripts/util.sh

# Prints last (newest) file identifier of the latest successful build
yatopia_get_last() {
  local jenkins_url="$(curl -s 'https://api.yatopiamc.org/v2/stableBuild' | jq -r '.jenkinsViewUrl')api/json"
  local file_name=$(curl -s "$jenkins_url" | jq -r '.artifacts[0].fileName')

  if [ "$file_name" != '' ]; then
    echo $file_name

    return 0
  fi

  return 1
}

# Prints the direct download link of the latest successful build
yatopia_get_link() {
  local direct_link=$(curl -s "https://api.yatopiamc.org/v2/stableBuild" | jq -r '.downloadUrl')

  if [ "$direct_link" != '' ]; then
    echo $direct_link

    return 0
  fi

  return 1
}

# $1: file_destination, default: $(yatopia_get_last)
yatopia_dl() {

  local file_destination=$1
  if [ "$file_destination" == '' ]; then
    file_destination=$(yatopia_get_last)

    if ! cmd_success; then
      return 1
    fi
  fi


  local direct_link=$(yatopia_get_link)

  if ! cmd_success; then
    return 1
  fi

  wget_dl "$direct_link" "$file_destination"

  return $?
}
