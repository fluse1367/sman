#!/bin/bash

# tmux utility functions

# Checks if a specific tmux session is running
# $1: tmux_session_name
tmux_is_session() {
  if [ -z "$1" ]; then
    return 1
  fi

  local sessions=$(tmux ls 2>&1)
  if ! cmd_success; then
    return 1
  fi

  local testline=$(echo "$sessions" | grep "$1")
  test -n "$testline"
  return $?
}

# Checks if a specific tmux session is not running
# If it's running, it prints an error message
# $1: tmux_session_name
# $2: server_name
tmux_test_is_not_session() {
  if (tmux_is_session $1); then
    echo "Error: '$2' is running."
    return 1
  fi
  return 0
}

# Checks if a specific tmux session is running
# If it's not running, it prints an error message
# $1: tmux_session_name
# $2: server_name
tmux_test_is_session() {
  if !(tmux_is_session $1); then
    echo "Error: '$2' is not running."
    return 1
  fi
  return 0
}

# Creates a detached tmux session with an initial command
# $1: tmux_session_name
# $2: exec
tmux_new_session() {
  tmux new -s "$1" -d "$2"
  return $?
}

# Sends keystrokes to a tmux session without submitting them
# $1: tmux_session_name
# $2: keys
tmux_send() {
  tmux send-keys -t "$1" "$2"
  return $?
}

# Sends keystrokes to a tmux session and submits them (Enter key)
# $1: tmux_session_name
# $2: keys
tmux_sendln() {
  tmux send-keys -t "$1" "$2" Enter
  return $?
}

# Attaches to a tmux session
# $1: tmux_session_name
tmux_attach() {
  tmux a -t "$1"
  return $?
}

# Forcibly terminates a tmux session
# $1: tmux_session_name
tmux_kill() {
  tmux kill-sess -t "$1"
  return $?
}
