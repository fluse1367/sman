#!/bin/bash

# functions to access papermc.io's api

# Prints last (newest) version identifier for desired subject
# $1: software_subject
pmc_get_last() {
  local ver=$(curl -s "https://papermc.io/api/v1/$1" | jq -r '.versions[0]')

  if [ "$ver" != '' ]; then
    echo $ver

    return 0
  fi

  return 1
}

# Prints the latest build number for desired subject / version
# $1: software_subject
# $2: version, default: $(pmc_get_last $software_subject)
pmc_get_last_build() {
  local software_subject=$1

  local version=$2
  if [ "$version" == '' ]; then
    version=$(pmc_get_last $software_subject)

    if ! cmd_success; then
      return 1
    fi

  fi

  local build=$(curl -s "https://papermc.io/api/v1/$software_subject/$version" | jq -r '.builds.latest')

  if [ "$build" != '' ]; then
    echo $build

    return 0
  fi

  return 1
}

# $1: software_subject
# $2: version, default: $(pmc_get_last $software_subject)
# $3: file_destination, default: ${software_subject}.jar
pmc_dl() {
  local software_subject=$1

  local version=$2
  if [ "$version" == '' ]; then
    version=$(pmc_get_last $software_subject)

    if ! cmd_success; then
      return 1
    fi

  fi

  local file_destination=$3
  if [ "$file_destination" == '' ]; then
    file_destination=${software_subject}.jar
  fi

  wget_dl "https://papermc.io/api/v1/$software_subject/$version/latest/download" "$file_destination"
}
