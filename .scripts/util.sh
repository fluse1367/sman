#!/bin/bash

# general utility functions

# Promts to continue
# Returns 0 on continue
# $1: promt_message
promptToYes() {
  while true; do

    echo -e "$1"
    read -r -n 1 -p "Continue? [Y/n] " input
    if [ ${#input} -eq 0 ]; then
      # enter was pressed
      return 0
    fi
    # print new line, cuz enter wasnt pressed
    echo
    case $input in
      [nN])
      return 1
      ;;
      [yY])
      return 0
      ;;
      *)
      clear
      echo -e "Invalid input: $input"
      ;;
    esac

  done
}

# Checks if packages are installed
# Returns 0 if (1) all packages are installed or (2) all packages could be installed
# $1: auto_install (1/0) - tries to auto install
# $2: packages - sparated by a space
package_check() {

  local packages=($(echo "${2}" | tr " " "\n"))

  local to_be_installed=()
  local package
  for package in "${packages[@]}"; do
    if [ $(dpkg-query -W -f='${Status}' $package 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
      to_be_installed+=($package)
    fi
  done



  if [ ${#to_be_installed[@]} -gt 0 ]; then
    echo "The following packages need to be installed:" ${to_be_installed[*]}

    if [ ${1} -ne 1 ]; then
      echo "Please install the packages."
      return 1
    fi

    local user=$(whoami)
    local sudoer=false
    if [ -n "$(id $user | grep sudo)" ]; then
      sudoer=true
    else
      if [ $user != "root" ]; then
        echo "You have insufficient permissions to install the needed packages."
        return 1
      fi
    fi

    local command="apt install -y $(echo ${to_be_installed[*]}) -y"
    if $sudoer; then
      command="sudo $command"
    fi

    if promptToYes "Do you want to install these packages? They may be crucial for executing this script."; then
      if ! $command; then
        echo "Failure during installation process was detected."
        return 1
      fi
      echo "The packages were successfully installed!"
    else
      echo "Cannot execute this script without the required packages."
      return 1
    fi

  fi

}

# Returns the last exit code
cmd_success() {
  return $?
}

# Downloads a file using wget while only showing it's progress bar
# $1: url
# $2: file dest
wget_dl() {
  wget -q --show-progress -O "$2" "$1"
}
