#!/bin/bash

# handler functions

source .scripts/util.sh
source .scripts/tmux_util.sh
source .scripts/controls.sh
source .scripts/yatopia_api.sh
source .scripts/buildtools.sh
source .scripts/papermc_api.sh

# $1: tmux_session_name
# $2: server_dir
handle_status() {
  if (tmux_test_is_session "$1" "$2"); then
    if [ -f $2/running ]; then
      echo "'$2' is running."
      return 0
    else
      echo "Session '$2' is running, but the process itself is terminated."
      return 1
    fi
  fi
}

# $1: tmux_session_name
# $2: server_dir
handle_start() {
  if (tmux_test_is_not_session "$1" "$2"); then
    echo "Starting ..."

    start "$1" "$2"
    sleep .5
    if tmux_is_session "$1" && [ -f "$2/running" ]; then
      echo "Server started!"
    else
      echo "It seems like the server could not be started properly."
      handle_status "$1" "$2"
    fi
  fi
}

# $1: tmux_session_name
# $2: server_name
handle_restart() {
  if (tmux_test_is_session "$1" "$2"); then
    restart "$1" "$2"
    echo "Restart command sent."
    return 0
  fi
}

# $1: tmux_session_name
# $2: server_dir
handle_stop() {
  if (tmux_test_is_session "$1" "$2"); then
    echo "Stopping the server ..."
    # send stop command
    stop $1 $2

    local delay=.1 # in seconds
    local max_wait=60 # in seconds

    local max_tick=$(echo "(1/$delay) * $max_wait" | bc)

    local tick=0
    while (tmux_is_session "$1"); do
      sleep $delay

      if [ $tick -ge $max_tick ]; then
        ((tick=-1))
        break
      fi
      ((tick+=1))

    done

    if [ $tick -eq -1 ]; then
      if promptToYes "'$1' could not be terminated within $max_wait second(s)! Force termination (just killing the process)?"; then
        if tmux_kill "$1"; then
          return 0
        else
          echo "Forced termination failed."
        fi
      fi
      return 1
    fi

    local real_time=$(echo "$tick * $delay" | bc)
    echo "'$2' successfully terminated within $real_time second(s)!"
    return 0

  fi
}

# $1: tmux_session_name
# $2: server_name
handle_attach() {
  if (tmux_test_is_session "$1" "$2"); then
    if promptToYes "You will enter a tmux session.
Press CTRL+B (then release) and hit D to exit the session.
Press CTRL+B (then release) and hit X to forcibly terminate the session.
Press CTRL+B (then release) and hit [ to enable scrolling. Hit q to exit scrolling."; then
      tmux_attach "$1"
    else
      echo "Abort."
    fi
  fi
}

# $1: tmux_session_name
# $2: server_dir
# $3: server_name
handle_backup() {
  if (tmux_test_is_not_session "$1" "$3"); then
    local name=backup-$3-$(date +%Y-%m-%d_%H-%M).tar.gz

    echo "Creating backup, please wait ..."
    cd $2
    tar -czf ../$name --totals *
    cd ..

    local size=$(ls -sh $name | cut -d " " -f 1)

    echo "Backup created: $name ($size)"
    return 0
  else
    echo "Please stop the server before creating a backup."
    return 1
  fi
}

# $1: server_dir
# $2: software_subject
# $3: memory
handle_install() {
  local server_dir="$1"
  local software_subject="$2"
  local dest="$server_dir/${2}.jar"

  if [ -f "$dest" ]; then
    echo "$dest already exists!"
    return 1
  elif ! [ -d "$server_dir" ]; then
    mkdir -p "$server_dir"
  fi
  case "$2" in
    "paper")
      local paper_ver=$(pmc_get_last "paper")
      local paper_build=$(pmc_get_last_build "paper" "$paper_ver")

      echo "Downloading paper-$paper_ver-$paper_build.jar into $dest ..."

      pmc_dl "$software_subject" "$paper_ver" "$dest"
    ;;
    "tuinity")
      echo "Downloading tuinity-paperclip.jar into $dest ..."
      wget_dl "https://ci.codemc.io/job/Spottedleaf/job/Tuinity/lastSuccessfulBuild/artifact/tuinity-paperclip.jar" "$dest"
    ;;
    "yatopia")
      local file_name=$(yatopia_get_last)

      echo "Downloading $file_name into $dest ..."

      yatopia_dl "$dest"
    ;;
    "spigot")
      if promptToYes "Spigot needs to be compiled first. That can take some time. If spigot was compiled before, the jar will just be copied.
Using tuinity or paper is recommended."; then
        cp_spigot_jar "$dest"
      fi
    ;;
    "waterfall")
      echo "Downloading waterfall.jar into $dest ..."

      pmc_dl "waterfall" "" "$dest"
    ;;
    "bungeecord")
      if !(package_check 1 "unzip"); then
        echo "'unzip' must be installed in order to extract the bungeecord artifacts archive."
        return 1
      fi

      local artifacts_file="$(dirname $dest)/artifacts.zip"
      echo "Downloading bungeecord artifacts into $artifacts_file ..."

      wget_dl "https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/artifact/*zip*/archive.zip" "$artifacts_file"
      if ! cmd_success; then
        echo "Download error!"
        stat "$artifacts_file"
        return 1
      fi

      echo "Extracting artifacts ..."
      local wd="$PWD"
      cd "$(dirname "$dest")"
      unzip "$(basename "$artifacts_file")" &> /dev/null
      find archive -name '*.jar' -exec cp {} ./ \;

      rm -r archive
      rm "$(basename "$artifacts_file")"
      mv BungeeCord.jar bungeecord.jar
      cd $wd
    ;;
    *)
    echo "Server type '$2' unknown."
    return 1
    ;;
  esac

  if cmd_success; then
    echo "Installation successful!"

    # create run file
    local run_file="$1/run.sh"
    if ! [ -f "$run_file" ]; then
      cp .runscripts/run.jvm.sh "$run_file"

      local op_memory="$3"
      if [ -z $op_memory ]; then
        op_memory="1G"
      fi
      local op_jar="$2.jar"
      local op_parameters=" nogui"
      sed -i -e "s/{{memory}}/$op_memory/g" "$run_file"
      sed -i -e "s/{{jar}}/$op_jar/g" "$run_file"
      sed -i -e "s/{{parameters}}/$op_parameters/g" "$run_file"
    fi

    # create halt file
    local halt_file="$1/halt.sh"
    if ! [ -f "$halt_file" ]; then
      cp .runscripts/halt.source.sh "$halt_file"
    fi

    return 0
  fi
  echo "An error occurred! The file might be corrupted."
  stat $dest

  return 1
}
