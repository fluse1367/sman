#!/bin/bash

# server control functions

# $1: tmux_session_name
# $2: server_dir
start() {
  # copy mantle run script
  cp .runscripts/mantle.sh $2/run.mantle.sh

  if ! cmd_success; then
    return 1
  fi

  cd $2
  # make sure the mantle script is executable
  chmod u+x run.mantle.sh

  # start tmux session
  tmux_new_session "$1" "./run.mantle.sh"
  cd ..
}

# $1: tmux_session_name
# $2: server_dir
restart() {
  source $2/halt.sh
}

# $1: tmux_session_name
# $2: server_dir
stop() {
  # create dontrestart file
  touch $2/dontrestart

  # send restart which (bc of dontrestart file) stops the server
  restart "$1" "$2"
}
