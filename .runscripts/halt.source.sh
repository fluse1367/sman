# generic halting script
# is being sourced by controls.sh in restart()

# $1: tmux_session_name
# $2: server_dir

tmux_sendln $1 "stop"
