#!/bin/bash

# mantle run script
# designed to repeatedly starting a program

scriptpath=$(readlink -f ${0})
# get sure, we are in the same directory as this script
cd `dirname $scriptpath`

# get sure this is not the sample script
dirname_only=$(basename $(dirname $scriptpath))
if [ $dirname_only == ".runscripts" ]; then
  echo "Please do not execute the sample script."
  exit 1
fi

while [ true ]; do

  # abort if no run file script be found
  if ! [ -r "run.sh" ]; then
    echo "WARNING: Cannot find compatible run script. Abort."
    exit 1
  fi

  touch running

  source run.sh

  rm running

  # after the process terminated, break the loop if file "dontrestart" exists
  if [ -f "dontrestart" ]; then
    echo "Stop command received, do not attempt restart."
    rm -f dontrestart
    break;
  fi

  # continue loop
  echo "The programm will be started again in 5 seconds. Press CTRL+C to abort (this will also terminate the session)."
  sleep 5
done

# self removal
rm $(basename $scriptpath)
