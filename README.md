# sman • the server-manager
sman is a bash script tool designed to ease the management of minecraft servers/proxies. But Basically it can run any application. Just provide a run and halt script for it.

### Disclaimer
This tool is written for Ubuntu/Debian. I do not provide any support on other Operating Systems.

Another thing: Note the [license of this project](./LICENSE). Use this script at your own risk! The developer(s) / contributors of this project do not take any responsibility/liability in any way.

### Usage

After cloning the repository, get sure the script is executable:
```bash
chmod u+x ./sman
```

Then you can already use it, type `./sman --help` and you'll the the help message:
```
Syntax: ./sman <server_name> <status|start|stop|restart|attach|backup|install [type] [memory]>

  <server_name> status  - Shows if the [server] is running.
  <server_name> start   - Starts the [server].
  <server_name> stop    - Stops the [server].
  <server_name> restart - Restarts the [server].
  <server_name> attach  - Attaches you to the [server]'s tmux session.
  <server_name> backup  - Creates a backup from [server]. Note: is must be shut down.
  <server_name> install [type] [memory] - Installs a server into '<server_name>'.
                  Possible types: yatopia, tuinity, paper, spigot, waterfall and bungeecord. Default server type: yatopia. Default memory: 1G.
```
#### Installing a new server
You can install a server by using:
```bash
./sman server_name install [server_type] [memory]
```
You can supply the following server types: [`yatopia`](https://yatopiamc.org/), [`tuinity`](https://github.com/Spottedleaf/Tuinity), [`paper`](https://papermc.io/), [`spigot`](https://www.spigotmc.org/wiki/spigot/), [`waterfall`](https://papermc.io/downloads#Waterfall) and [`bungeecord`](https://www.spigotmc.org/wiki/bungeecord/). Default server type is `yatopia`, default memory is `1G`.<br>
sman will download the required files and place them into a new directory (the name will be your server name).

<b>Please note</b> when using `spigot` as server type: The script will download BuildTools and compile spigot by itself. Read more about this [here](https://www.spigotmc.org/threads/buildtools-updates-information.42865/) and [here](https://www.spigotmc.org/threads/bukkit-craftbukkit-spigot-1-8.36598/).

#### Installing a new server or application manually
Of course you can also install new servers by yourself!

Basically, sman just looks for a run and halt script within the server's directory (`run.sh`, `halt.sh`).

In order to make a server by yourself, create a new directory. Call it how you want (but, without spaces!). Then you can place your application files in there!<br>

The second step is to create a `run.sh` script in there, which will be executed by the mantle script (The mantle script only gets sure the application will be restarted).
The run script doesn't have to be executable as it wont be executed directly.

As third step you have to create a `halt.sh` script in there, which will halt the application. It don't have to be executable as it won't be executed directly. It will be sourced inside the `restart()` function in [`controls.sh`](.scripts/controls.sh), thus the executed code will have access to the parameters `$1` (the tmux session name) and `$2` (the server directory). Take a look at [`halt.source.sh`](.runscripts/halt.source.sh) (and maybe at [`tmux_util.sh`](.scripts/tmux_util.sh) as well) to get an idea of how to send commands to tmux sessions.

That's it! Now you can start your server. Note: with custom run scripts you can basically run anything with sman. It's not limited to java or minecraft applications.

#### Starting a server
When typing
```bash
./sman server_name start
```
sman will indirectly run a file called `run.sh` within the server's directory. You can modify it if you want.

#### Accessing the console
With
```bash
./sman server_name attach
```
you can enter the server's console. Before you will be granted with a little information block:
```
You will enter a tmux session.
Press CTRL+B (then release) and hit D to exit the session.
Press CTRL+B (then release) and hit X to forcibly terminate the session.
Press CTRL+B (then release) and hit [ to enable scrolling. Hit q to exit scrolling.
Continue? [Y/n]
```
Pressing `y` or Enter will continue. `n` will abort.
As the block says, you can hit certain combinations to achieve different actions. They all have `CTRL+B` in common, meaning you have to hit the `CTRL` key with the `B` key at the same time. Then release the keys. Tmux will now listen for commands, hitting ...
 - `D` will exit the console. The server will continue to run in the background.
 - `[` will take you into the scrolling mode. Here you can use the arrow keys, the mouse-wheel or the page up/down keys to scroll through the past output. You can exit the scrolling mode just by hitting `q`.
 - `X` will forcibly terminate the console/session, **this means it immediately kills the server process! This can lead to data loss and/or data corruption!**

[Here](https://tmuxcheatsheet.com/) you can access a quick reference for tmux commands.
